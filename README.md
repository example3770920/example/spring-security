# 프로젝트 개요

## 기능:
- JWT 기반으로 사용자 인증 및 권한 관리 수행
- Stateless 인증 방식 적용 (세션을 사용하지 않음)
- HTTP 요청 시, JWT 토큰을 검증하여 사용자 인증 처리

## 구성 요소:
1. **JwtTokenProvider**: JWT 토큰 생성 및 검증
2. **JwtAuthenticationFilter**: HTTP 요청 필터에서 JWT 토큰 확인
3. **WebSecurityConfiguration**: Spring Security 설정

---

## 구성 파일 및 주요 기능

### 1️⃣ JwtTokenProvider (JWT 관리)

**위치**: `com.example.security.core.security.jwt.JwtTokenProvider`

#### 기능:
- **JWT 생성 (`createToken`)**  
  사용자 ID(userPk)와 권한(roles) 정보를 JWT에 포함하여 토큰을 생성.  
  유효 시간(tokenValidTime) 이후 토큰 만료.

- **JWT 토큰에서 사용자 정보 추출 (`getUserPk`)**  
  JWT 토큰에서 사용자 ID를 추출.

- **JWT 인증 정보 조회 (`getAuthentication`)**  
  JWT 토큰에서 인증 정보를 조회.

- **JWT 유효성 검사 (`validateToken`)**  
  JWT 토큰의 유효성을 검사.

- **HTTP 요청에서 토큰 추출 (`resolveToken`)**  
  HTTP 요청에서 JWT 토큰을 추출.

#### `createToken(userPk, roles)`:
- 사용자 ID(userPk)와 권한(roles) 정보를 포함하여 JWT를 생성.
- 유효 시간(tokenValidTime) 이후 토큰 만료.

---

### 2️⃣ JwtAuthenticationFilter (JWT 필터)

**위치**: `com.example.security.core.security.filter.JwtAuthenticationFilter`

#### 기능:
- HTTP 요청의 Authorization 헤더에서 JWT 토큰을 추출.
- 토큰 검증 후 SecurityContext에 사용자 인증 정보를 저장.

#### `doFilter(...)`:
- Authorization 헤더에서 JWT 토큰 추출.
- 토큰이 유효하면 Authentication 객체를 생성 후 SecurityContext에 저장.
- 이후 필터 체인을 계속 진행.

---

### 3️⃣ WebSecurityConfiguration (Spring Security 설정)

**위치**: `com.example.security.configurations.WebSecurityConfiguration`

#### 기능:
- Spring Security를 설정하고 JwtAuthenticationFilter를 추가.
- 세션을 사용하지 않는 Stateless 인증 방식 설정.
- `/login`, `/error` 경로는 인증 없이 접근 가능.
- 기타 모든 요청은 JWT 인증을 요구.

#### `filterChain(HttpSecurity http)`:
- CSRF 보호 비활성화 (`http.csrf().disable()`)
- 세션 사용 안함 (`SessionCreationPolicy.STATELESS`)
- `/login` 및 `/error` 요청은 인증 없이 허용.
- JwtAuthenticationFilter를 `UsernamePasswordAuthenticationFilter` 이전에 추가.
