package com.example.security.features.hello;

import com.example.security.core.domain.User;
import com.example.security.core.security.jwt.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RequiredArgsConstructor
@RestController
public class HelloResource {

    private final JwtTokenProvider jwtTokenProvider;


    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody User user) {
        /*
        log.info("user email = {}", user.get("email"));
        User member = userRepository.findByUserEmail(user.get("email"))
                .orElseThrow(() -> new IllegalArgumentException("가입되지 않은 E-MAIL 입니다."));
        */
        return ResponseEntity.ok(jwtTokenProvider.createToken(user.getId(),null));
    }

    @PostMapping("/test")
    public ResponseEntity<String> test() {

        return ResponseEntity.ok("ok");
    }
}
